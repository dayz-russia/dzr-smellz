modded class AnimalBase
{
	int theTimestamp
	
	//zombie + animals
	//smell, timestamp, intensity
	ref map<int, int > objectSmells = new map<int, int >;
	
	ref map< string, ref map<int, int> > existingSmells = new map< string, ref map<int, int> >;
	//ref map<int, int> smellProperties = new map<int, int>;
	
	ref array<Param8<int, string, float, int, int, int, int, int>> m_ObjectSmells = new array<Param8<int, string, float, int, int, int, int, int>>;
	
	void AnimalBase()
	{
		
		int hour;
		int minute;
		int second;
		int year;
		int month;
		int day;
		
		GetHourMinuteSecond(hour, minute, second);
		GetYearMonthDay(year, month, day);
		//Print( (year + month + day).ToString() +  (hour/60 +  minute/60 +  second).ToString() );
		
		theTimestamp = ( (year + month + day).ToString() +  (day +  minute +  second).ToString() ).ToInt() ;
		
		//Print("theTimestamp: "+theTimestamp);
		//set default infinite smells
		/*
			ref SmellProto<int, string, float, int, int, int, int, int>InitialSmell = Smell.AnimalDomestic;
			ref SmellProto<int, string, float, int, int, int, int, int> NewSmell = new SmellProto<int, string, float, int, int, int, int, int>(InitialSmell.id, InitialSmell.name, InitialSmell.intensity, InitialSmell.localSpreadRadius, InitialSmell.maxDistance, InitialSmell.category, InitialSmell.color, InitialSmell.timestamp );
			
			m_ObjectSmells.Insert( NewSmell );
			
			
			Print("m_ObjectSmells:" + m_ObjectSmells[0]);
		*/
		
	}
	
	//ref array<Param8<int, string, float, int, int, int, int, int>> getSmells()	
	ref map< string, ref map<int, int> > getSmells()	
	{
		/*
			ref array < array< int, float> > objArray = objectSmells.GetValueArray();
			Print("objectSmells: "+ objArray[0]);
			Print("The smell: " + objectSmells.GetKey(0) );
			
			ref Param3<string, int, float> m_Data = new Param3<string, int, float>("#STR_SMELLZ_ANIMAL",COLOR_YELLOW, objArray[0][1]);
			GetRPCManager().SendRPC( "DZR_SMELLZ_RPC", "DZR_ReceiveSmellFromObject", m_Data, true, NULL);
		*/
		/*		 
			ref Param8<int, string, float, int, int, int, int, int> NewSmell = new Param8<int, string, float, int, int, int, int, int>( Smell.AnimalDomestic.id, Smell.AnimalDomestic.name, Smell.AnimalDomestic.intensity, Smell.AnimalDomestic.localSpreadRadius, Smell.AnimalDomestic.maxDistance, Smell.AnimalDomestic.category, Smell.AnimalDomestic.color, Smell.AnimalDomestic.timestamp );
		*/
		
		ref map<int, int> smellProperties = new map<int, int>;
		
		existingSmells.Clear();
		smellProperties.Clear();
		
		if( IsAlive() )
		{
			if(IsDanger())
			{
				smellProperties.Insert(Smell.AnimalWild.category, Smell.AnimalWild.intensity);
				existingSmells.Insert( Smell.AnimalWild.name, smellProperties );
			}
			else
			{
				smellProperties.Insert(Smell.AnimalDomestic.category, Smell.AnimalDomestic.intensity);
				existingSmells.Insert( Smell.AnimalDomestic.name, smellProperties );
			}
		}
		else
		{
			
			smellProperties.Insert(Smell.CorpseSmall.category, Smell.CorpseSmall.intensity);
			existingSmells.Insert( Smell.CorpseSmall.name, smellProperties );
		}
		//existingSmells.Insert( "Strange", 9, 2 );
		
		
		return existingSmells;
		//return m_ObjectSmells;
	}
	
	/*
		void addSmell(int newSmell, float intensity)
		{
		// if exists
		bool smellExists = objectSmells.Find(newSmell, existingSmell);
		if( smellExists ) 
		{
		
		// update timestamp
		existingSmell[0] = theTimestamp;
		
		//if old intensity is lower
		if(existingSmell[1] < intensity)
		
		{
		// update intensity
		existingSmell[1] = intensity;
		
		}
		objectSmells.Set(newSmell, existingSmell );
		}
		// insert new smell
		objectSmells.Insert( newSmell, {theTimestamp, intensity} );
		Print(objectSmells);
		}
		
		void clearSmells()
		{
		//empty array
		}
	*/
}