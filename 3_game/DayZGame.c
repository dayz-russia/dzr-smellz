class SmellCategory
{
	
	const int RED_COLOR = ARGB(255, 255, 0, 0);
	const int ORANGE_COLOR = ARGB(255, 209, 108, 0);
	const int LIME_COLOR = ARGB(255, 0, 255, 0);
	const int DEFAULT = ARGB(100, 255, 255, 255);
	const int SKYBLUE_COLOR = ARGB(255, 0, 162, 255);
	const int DARKYELLOW_COLOR = ARGB(255, 255, 192, 0);
	
	const int RED					= 0xFF0000;
	const int Disturbing			= RED;
	const int CYAN					= 0x00FFFF;
	const int GRAY					= 0xBEBEBE;
	const int GREEN					= 0x00FF00;
	const int PURPLE				= 0xA020F0;
	const int YELLOW				= 0xFFFF00;
	const int Alarming				= YELLOW;
	const int ORANGE				= 0xFFA500;
	const int BLUE					= 0x0000FF;
	const int Interesting			= BLUE;
	const int BLACK					= 0x000000;
	const int BROWN					= 0xA52A2A;
	const int WHITE					= 0xFFFFFF;
	const int WHITEGRAY				= 0xA0A1A1;
	
	const int COLOR_DEFAULT			= 0x64000000;

	const int COLOR_RUINED			= 0x00FF0000;
	const int COLOR_BADLY_DAMAGED	= 0x00FFBF00;
	const int COLOR_DAMAGED			= 0x00FFFF00;
	const int COLOR_WORN			= 0x00BFFF00;
	const int COLOR_PRISTINE		= 0x0040FF00;

	const int COLOR_DRENCHED		= 0x000000FF;
	const int COLOR_SOAKING_WET 	= 0x003030FF;
	const int COLOR_WET 			= 0x006060FF;
	const int COLOR_DAMP 			= 0x009090FF;

	const int COLOR_LIQUID 			= 0x0000EEFF;
	
	const int COLOR_RAW				= 0x00BF4242;
	const int COLOR_BAKED			= 0x00A56D28;
	const int COLOR_BOILED			= 0x00AF9442;
	const int COLOR_DRIED			= 0x00FFEB48;
	const int COLOR_BURNED			= 0x00888888;
	const int COLOR_ROTTEN			= 0x00A93F15;
}

class Smell
{
	static int			id = 0;
	static string		name = "Default Smell";
	static float		intensity = 2.0;
	static int			localSpreadRadius = 7;
	static int			maxDistance = 200;
	static int			category = 1;
	static int			color = COLOR_YELLOW;
	static int			timestamp = 0;
	
	const static ref SmellProto<int, string, float, int, int, int, int, int> DefaultSmell = new SmellProto<int, string, float, int, int, int, int, int>(id, name, intensity, localSpreadRadius, maxDistance, category, color, timestamp);
	
	const static ref SmellProto<int, string, float, int, int, int, int, int> Animal = new SmellProto<int, string, float, int, int, int, int, int, int>(1, "#STR_SMELLZ_ANIMAL", 4.0, 15, 200, 6, COLOR_RED, 0);	
	
	const static ref SmellProto<int, string, float, int, int, int, int, int> AnimalDomestic = new SmellProto<int, string, float, int, int, int, int, int, int>(2, "#STR_ANIMAL_DOMESTIC", 3.0, 15, 200, 6, 1, 0);
	
	const static ref SmellProto<int, string, float, int, int, int, int, int> AnimalWild = new SmellProto<int, string, float, int, int, int, int, int, int>(3,"#STR_ANIMAL_WILD", 3.0, 15, 200, 8, 1, 0);
	
	const static ref SmellProto<int, string, float, int, int, int, int, int> CorpseSmall = new SmellProto<int, string, float, int, int, int, int, int, int>(4,"#STR_CORPSE_SMALL", 3.0, 15, 200, 12,1 , 0);	
	
	const static ref SmellProto<int, string, float, int, int, int, int, int> CorpseBig = new SmellProto<int, string, float, int, int, int, int, int, int>(5,"#STR_CORPSE_BIG", 3.0, 15, 200, 12, 1, 0);	
}

class SmellProto<Class T1, Class T2, Class T3, Class T4, Class T5, Class T6, Class T7, Class T8>: Param
{
    T1 id;
    T2 name;
    T3 intensity;
    T4 localSpreadRadius;
    T5 maxDistance;
    T6 category;
    T7 color;
    T8 timestamp;

    void SmellProto(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8)
    {
        id = p1;
        name = p2;
        intensity = p3;
        localSpreadRadius = p4;
        maxDistance = p5;
        category = p6;
        color = p7;
        timestamp = p8;
    }
        
    override bool Serialize(Serializer ctx)
    {
        return ctx.Write(id) && ctx.Write(name) && ctx.Write(intensity) && ctx.Write(localSpreadRadius) && ctx.Write(maxDistance) && ctx.Write(category) && ctx.Write(color) && ctx.Write(timestamp);
    }
        
    override bool Deserializer(Serializer ctx)
    {
        return ctx.Read(id) && ctx.Read(name) && ctx.Read(intensity) && ctx.Read(localSpreadRadius) && ctx.Read(maxDistance) && ctx.Read(category) && ctx.Read(color) && ctx.Read(timestamp);
    }
};