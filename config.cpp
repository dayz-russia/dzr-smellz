class CfgPatches
{
	class dzr_smellz
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_smellz
	{
		type = "mod";
		author = "DayZRussia";
		description = "Olifaction mod";
		dir = "dzr_smellz";
		name = "dzr_smellz";
		//inputs = "dzr_smellz/Data/Inputs.xml";
		dependencies[] = {"Game","World","Mission"};
		class defs
		{
			class gameScriptModule
			{
				files[] = {"dzr_smellz/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_smellz/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_smellz/5_Mission"};
			};
		};
	};
};
