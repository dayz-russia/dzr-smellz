/*
	TODO:
	- Entity pings players
*/
modded class MissionGameplay
{
	//simple change
	int RED_COLOR = ARGB(255, 255, 0, 0);
	int ORANGE_COLOR = ARGB(255, 209, 108, 0);
	int LIME_COLOR = ARGB(255, 0, 255, 0);
	int DEFAULT = ARGB(100, 255, 255, 255);
	int SKYBLUE_COLOR = ARGB(255, 0, 162, 255);
	int DARKYELLOW_COLOR = ARGB(255, 255, 192, 0);
	
	vector g_WindDirRealVector;
	vector g_WindDirVector;
	float g_WindRotation;
	float g_WindSpeed;
	float g_WindAngle;
	float g_ReductionCoef = 1;
	float g_WindSpeedMax;
	bool g_SpreadSmell	 = false;
	Weather weather;
	protected Widget m_DZRsmellz_root;
	protected ImageWidget m_SmellPanel;
	protected ImageWidget m_Smell;
	protected ImageWidget m_Dot;
	protected ImageWidget m_WindDir;
	protected ImageWidget m_Wind1;
	protected ImageWidget m_Wind2;
	protected ImageWidget m_Wind3;
	protected RichTextWidget m_Debug1;
	protected RichTextWidget m_Debug2;
	bool smellzActive = false;
	
	int CollectSmellsInterval = 2000;
	int UpdateWindInterval = 50;
	
	ref array<Param> m_Receptors = new array<Param>;
	
	ref array<Object> smellyObjects = new array<Object>;
	//ref array<Object, int, float> smellz = new array<Object>;
	
	void MissionGameplay()
	{
		
		Print("[DZR SmellZ] ::: Starting ");
		weather = GetGame().GetWeather();
		m_DZRsmellz_root = GetGame().GetWorkspace().CreateWidgets( "dzr_smellz/GUI/layouts/smellz.layout" );
		
		m_SmellPanel = Widget.Cast( m_DZRsmellz_root.FindAnyWidget("m_SmellPanel") );
		m_Smell = Widget.Cast( m_DZRsmellz_root.FindAnyWidget("m_Smell") );
		m_Dot = Widget.Cast( m_DZRsmellz_root.FindAnyWidget("m_Dot") );
		m_WindDir = Widget.Cast( m_DZRsmellz_root.FindAnyWidget("m_WindDir") );
		m_Wind1 = Widget.Cast( m_DZRsmellz_root.FindAnyWidget("m_Wind1") );
		m_Wind2 = Widget.Cast( m_DZRsmellz_root.FindAnyWidget("m_Wind2") );
		m_Wind3 = Widget.Cast( m_DZRsmellz_root.FindAnyWidget("m_Wind3") );
		m_Debug1 = TextWidget.Cast( m_DZRsmellz_root.FindAnyWidget("m_Debug1") );
		m_Debug2 = TextWidget.Cast( m_DZRsmellz_root.FindAnyWidget("m_Debug2") );
		
		m_DZRsmellz_root.Show(true);
		m_SmellPanel.Show(false);	
		m_Smell.Show(false);	
		m_Dot.Show(false);	
		m_WindDir.Show(false);	
		m_Wind1.Show(false);	
		m_Wind2.Show(false);	
		m_Wind3.Show(false);	
		m_Debug1.Show(true);	
		m_Debug2.Show(true);	
		m_Debug2.SetAlpha(0);	
		
		m_Wind1.SetColor(DEFAULT);
		m_Wind2.SetColor(DEFAULT);
		m_Wind3.SetColor(DEFAULT);
		
		GetRPCManager().AddRPC( "DZR_SMELLZ_RPC", "DZR_ReceiveSmellFromObject", this, SingleplayerExecutionType.Both );
		/*
			ref SmellProto<int, string, float, int, int, int, int, int> NewSmell = new SmellProto<int, string, float, int, int, int, int, int>(Smell.AnimalDomestic.id, Smell.AnimalDomestic.name, Smell.AnimalDomestic.intensity, Smell.AnimalDomestic.localSpreadRadius, Smell.AnimalDomestic.maxDistance, Smell.AnimalDomestic.category, Smell.AnimalDomestic.color, Smell.AnimalDomestic.timestamp, );
			
			NewSmell.name = "#NEW";
			NewSmell.intensity = 4;
		*/
		// m_Receptors.InsertAt( NewSmell, 1 );
		
		//ReceptorsSendNewSmell(Smell.AnimalDomestic);
		//ReceptorsSendNewSmell(Smell.AnimalWild);
		//ReceptorsSendNewSmell(NewSmell);
		//ReceptorsSendNewSmell(Smell.AnimalDomestic);
		//ReceptorsSendNewSmell(Smell.AnimalDomestic);
		
		
		
		//ReceptorsSendNewSmell(NewSmell);
		
	}
	
	
	
	void ActivateSmellZui(bool setting)
	{
		smellzActive = true;	
		m_SmellPanel.Show(setting);	
		//m_Smell.Show(setting);	
		m_Dot.Show(setting);	
		m_WindDir.Show(setting);	
		m_Wind1.Show(setting);	
		m_Wind2.Show(setting);	
		m_Wind3.Show(setting);		
	}
	
	void DZR_ReceiveSmellFromObject( CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target )
	{
		Param3<string, int, float> data;
		//smell, intensity
		if ( !ctx.Read( data ) ) return;
		
		if (type == CallType.Client)
		{
			string smellName = data.param1;
			int smellCategory = data.param2;
			float smellIntensity = data.param3;
			
			m_Debug2.SetAlpha(smellIntensity);
			m_Debug2.SetColor(smellCategory);
			m_Debug2.SetText(smellName + " ("+smellIntensity+")");
		};
	};
	
	
	void ~MissionGameplay()
	{
		//dzr_Print("DZR_RecognitionToggle: "+DZR_RecognitionToggle);
		//dzr_Print("Remove(this.RayCastForPlayer)");
		//DZR_RecognitionToggle = false;
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.SmellTextAlpha1);
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.SmellTextAlpha2);
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.RemoveSmellText);
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.UpdateSmells);
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.UpdateWind);
		//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.updateSelfStates);
		m_DZRsmellz_root.Show(false);
		m_DZRsmellz_root = null;
		
		
	}
	
	void SmellTextAlpha1()
	{
		
				
				GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.SmellTextAlpha2);
				GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.RemoveSmellText);
		
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.SmellTextAlpha2, 3500, false);
		m_Debug2.SetAlpha(0.65);
		//m_Debug2.SetColor(DEFAULT);
	}
	
	void SmellTextAlpha2()
	{
		m_Debug2.SetAlpha(0.42);
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.RemoveSmellText, 2000, false);
	}
	
	void RemoveSmellText()
	{
		m_Debug2.SetText("");
		m_Debug2.SetColor(DEFAULT);
		//m_Debug2.SetOutline(0, 0x000000);
		//m_Debug2.SetShadow(0,0xFF000000, 0, 0, 0);
	}
	
	void smellSmoke(Object object, string suffix = "")
	{
		array<int> CategoryColor = {
			
			Colors.BLACK, //0
			Colors.BLACK, //1
			Colors.GRAY, //2
			Colors.WHITEGRAY, //3
			Colors.BROWN, //4
			Colors.GREEN, //5
			ARGB(255, 0, 162, 255), //6
			Colors.ORANGE, //7
			Colors.YELLOW, //8
			Colors.PURPLE, //9
			Colors.CYAN, //10
			Colors.PURPLE, //11
			ARGB(255, 255, 0, 0) //12
		}
		
		FireplaceBase fireplace_target = FireplaceBase.Cast( object );
		if ( object )
		{
			PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
			vector playerPos = player.GetPosition();
			vector objectPos = object.GetPosition();
			vector dirToPlayer = vector.Direction(objectPos, playerPos).VectorToAngles();
			float m_DistToPlayer = vector.Distance(objectPos, playerPos );
			//if( (g_WindDirVector[0] > ( dirToPlayer[0] - g_WindAngle) ) && (g_WindDirVector[0] < ( dirToPlayer[0] + g_WindAngle) ) ) 
			if( ( (g_WindDirVector[0] > ( dirToPlayer[0] - g_WindAngle) ) && (g_WindDirVector[0] < ( dirToPlayer[0] + g_WindAngle) ) ) || g_SpreadSmell || m_DistToPlayer <= 7 ) 
			{
				
				if( isObjectClass(object, "AnimalBase")  )
				{
					
					//smellShown = true;
					
					
					//objectSmells.Find(newSmell, existingSmell);
					AnimalBase animalObject = AnimalBase.Cast( object );
					ref map< string, ref map<int, int> > smellsArray = animalObject.getSmells();
					//ref array<string> smellsArray = animalObject.getSmells();
					Print(smellsArray);
					if(smellsArray)
					{
						
						//ref Param8<int, string, float, int, int, int, int, int>Smell1 = smellsArray[0]; 
						//ref Param8<int, string, float, int, int, int, int, int>Smell1 = smellsArray[0]; 
						/*
							string smellName = Smell1.name;
							int smellCategory = Smell1.category;
							float smellIntensity = Smell1.intensity;
							
							m_Debug2.SetAlpha(smellIntensity);
							m_Debug2.SetColor(smellCategory);
							m_Debug2.SetText(smellName + " ("+smellIntensity+")");
							ReceptorsSendNewSmell(Smell1);
						*/
						
						string smellName = smellsArray.GetKey(0);
						int theCategory = smellsArray.GetElement(0).GetKey(0);
						float smellIntensity = smellsArray.GetElement(0).GetElement(0);
						
						m_Debug2.SetAlpha(1);
						//m_Debug2.SetColor(SKYBLUE_COLOR);
						//m_Debug2.SetOutline(2, 0x000000);
						//m_Debug2.SetShadow(10,0xFF000000, 1, 0, 0);
						m_Debug2.SetColor(CategoryColor[theCategory]);
						m_Debug2.SetText(smellName);
						//m_Debug2.SetText(smellName + " ("+smellIntensity+")");
						//ReceptorsSendNewSmell(Smell1);
						
						//GetGame().Chat("Smelly Animal Nearby: " + smellName + " - "+theCategory+" - "+smellIntensity, "colorImportant" );
						//GetGame().Chat("Smelly Animal Nearby: " + smellsArray[0], "colorImportant" );
						
						GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.SmellTextAlpha1);
						
					}
					
					
					
				}
				
				if ( fireplace_target )
				{
					
					if ( fireplace_target.HasAshes() && !fireplace_target.IsBurning() )
					{
						//return 2;
						//m_Smell.Show(true);	
						m_Debug2.SetAlpha(1);
						m_Debug2.SetColor(ORANGE_COLOR);
						//m_Debug2.SetText("You smelled old burnt wood. ("+g_SpreadSmell+":"+m_DistToPlayer+")");
						m_Debug2.SetText("#STR_SMELLZ_BURNT");
						//m_Debug2.SetShadow(9,0xFF000000, 1, 0, 0);
						//Print("Found old fire "+suffix+" DIR:"+Math.Round(dirToPlayer[0]));
						GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.SmellTextAlpha1);
					}
					
					if ( fireplace_target.IsBurning() )
					{
						//return 1;
						//active fire
						//m_Smell.Show(true);	
						m_Debug2.SetAlpha(1);
						m_Debug2.SetColor(RED_COLOR);
						m_Debug2.SetText("#STR_SMELLZ_SMOKE");
						//m_Debug2.SetShadow(9,0xFF000000, 1, 0, 0);
						//Print("Found burning fire "+suffix+" DIR:"+Math.Round(dirToPlayer[0]));
						GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.SmellTextAlpha1);
					}
				}
				

				GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.SmellTextAlpha1, 5600, false);
				//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.SmellTextAlpha1, 6000, false);
				
			}
		}
		else {
			m_Smell.Show(false);
			//TODO: Create array os received smells, timeout smells
			
		}
		//return 1;	
	}
	
	void updateWindSpeed()
	{
		m_Wind1.Show(false);
		m_Wind2.Show(false);	
		m_Wind3.Show(false);
		if( (g_WindSpeed <= 7) )
		{
			m_Wind1.Show(true);
		}
		if( (g_WindSpeed > 7) && (g_WindSpeed <= 14) )
		{
			m_Wind2.Show(true);
		}
		if( (g_WindSpeed > 14) )
		{
			m_Wind3.Show(true);
		}
		
	}
	
	
	void UpdateSmells()
	{
		PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
		//vector m_playerOrientation = GetGame().GetPlayer().GetOrientation();
		
		//GetGame().GetCurrentCameraPosition() + GetGame().GetCurrentCameraDirection()
		//vector m_OrientationAV = GetGame().GetPlayer().GetOrientation().AnglesToVector();
		
		if(player)
		{
			
			//SMEELING NEAREST OBJECTS
			ref array<Object> nearest_objects = new array<Object>;
			ref array<CargoBase> proxy_cargos = new array<CargoBase>;
			vector pos = player.GetPosition();
			
			float maxDistance = 450;
			float minDistance = 50;
			float dPercent = (maxDistance - minDistance) / g_WindSpeedMax;
			float calcDistance = 50 + dPercent * g_WindSpeed ; //TODO: Need to reduce each smell individually and measure distance.
			
			GetGame().GetObjectsAtPosition3D( pos, calcDistance, nearest_objects, proxy_cargos );
			//GetGame().Chat("DIST: "+Math.Round(calcDistance)+" ("+Math.Round(g_WindSpeed)+"mps|"+Math.Round(g_WindAngle)+"°)", "colorImportant");
			//m_Debug1.SetText("DIST: "+Math.Round(calcDistance)+" ("+Math.Round(g_WindSpeed)+"mps|"+Math.Round(g_WindAngle)+"°)");
			//m_DistToObject = vector.Distance(pos);
			if (g_WindSpeed <= 5)
			{
				g_SpreadSmell = true;
			} else 
			{
				g_SpreadSmell = false;
			}
			
			for ( int i = 0; i < nearest_objects.Count(); ++i )
			{
				Object object = nearest_objects.Get( i );
				//sendSmellsToReceptors(object, "in radius");
				smellSmoke(object, "in radius");
				//Print("In range: "+object);
			}
			
			
			//SMELLING WIND
			
		}
		
	}
	
	bool isObjectClass(Object theObject, string className)
	{
		
		if ( theObject.IsKindOf(className) )
		{
			return true;
		}
		return false;
	}
	
	void sendSmellsToReceptors(ref array<Object> nearest_objects)
	{
		bool smellShown;
		bool animalAround = true;
		PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
		/**	
			TODO: 
			Add from hands of players too
			Remove by timer
			Store intensity
		*/
		int hour;
		int minute;
		int second;
		int year;
		int month;
		int day;
		
		GetHourMinuteSecond(hour, minute, second);
		GetYearMonthDay(year, month, day);
		Print( (year + month + day).ToString() +  (hour/60 +  minute/60 +  second).ToString() );
		
		int theTimestamp = ( (year + month + day).ToString() +  (day +  minute +  second).ToString() ).ToInt() ;
		// primary_receptor_array 4 slots
		// find smells in the nearest objects by type
		// found smell
		// smell exists on the receptors?
		//int smellTimestamp;
		//bool m_SmellExists = m_Receptors.Find(objectSmell, smellTimestamp);
		// new timestamp
		//m_Receptors.Set(objectSmell, theTimestamp);
		// compare intensity
		
		// if bigger
		// new intensity 
		// old intensity
		// update with new intensity and timestamps
		// new smell
		// insert into receptors
		// refresh UI
		// if smell timestamp delta = 5
		// reduce intensity by 1
		// sort smells by intensity
		// update receptor1, receptor2, receptor3, receptor4
		
		ref map<int, ref array< int, float> > objectSmells;
		ref map<string, ref array< int, float> > objectSmells2;
		//Print("sending smells ");
		
		
		
		
		vector playerPos = player.GetPosition();
		
		
		
		
		
		
		for ( int i = 0; i < nearest_objects.Count(); ++i )
		{
			
			Object object = nearest_objects.Get( i );
			smellSmoke(object, "in radius");
			//Print("In range: "+object);
			//no smell in gasmask
			//if(FullMaskClassnames.Find(classname) != -1) {
			
			
			
		}
		/*
			GetGame().Chat( player.GetSimulationTimeStamp().ToString() + " ITEM1: "+smellyObjects.Get( 0 ), "colorImportant");
			GetGame().Chat( player.GetSimulationTimeStamp().ToString() + " ITEM2: "+smellyObjects.Get( 1 ), "colorImportant");
			GetGame().Chat( player.GetSimulationTimeStamp().ToString() + " ITEM3: "+smellyObjects.Get( 2 ), "colorImportant");
			GetGame().Chat( player.GetSimulationTimeStamp().ToString() + " ITEM4: "+smellyObjects.Get( 3 ), "colorImportant");
		*/
	}
	void UpdateWind ()
	{
		PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
		//max wind speeed 20
		vector m_playerOrientation = GetGame().GetCurrentCameraDirection().VectorToAngles();
		g_WindDirRealVector = weather.GetWind();
		g_WindDirVector = weather.GetWind().VectorToAngles() + "0 -90 0";
		g_WindSpeed =  weather.GetWindSpeed();
		g_WindSpeedMax = weather.GetWindMaximumSpeed();
		
		
		float maxAngle = 360;
		float minAngle = 32;
		float aPercent = ( (maxAngle - minAngle) / 2 ) / g_WindSpeedMax;
		//g_WindAngle = 16 + ( ( 360/2 - 16 ) / g_WindSpeedMax ) * g_WindSpeed;
		g_WindAngle = ( (maxAngle + minAngle) / 2 - aPercent * g_WindSpeed ) / 2;
		
		m_WindDir.SetRotation( 0, 0, (g_WindDirVector[0] + (360 - m_playerOrientation[0]) ) - 180 );
		
		updateWindSpeed();
	}
	
	override void OnUpdate(float timeslice)
	{
		super.OnUpdate( timeslice );
		PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
		
		if(player) {
			if(!smellzActive){
				ActivateSmellZui(true);
				GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.UpdateSmells, CollectSmellsInterval, true);
				GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.UpdateWind, UpdateWindInterval, true);
				smellzActive = true;
			}		
			
			
		}
	}	
	
	
	void ReceptorsSendNewSmell (ref SmellProto<int, string, float, int, int, int, int, int> NewSmell)
	{
		int ReceptorIndex;
		int freeReceptorIndex = 0;
		if( SmellExists(NewSmell.id, ReceptorIndex, freeReceptorIndex) )
		{
			Print("Smell " + NewSmell.name + " exists.");
			ReceptorUpdateSmell(ReceptorIndex, NewSmell);
		}
		else
		{
			
			Print( NewSmell.name + " added.");
			m_Receptors.InsertAt( NewSmell, freeReceptorIndex);
			//m_Receptors[freeReceptorIndex] = NewSmell;
			
		}
	}
	
	void ReceptorUpdateSmell(int ReceptorIndex, ref SmellProto<int, string, float, int, int, int, int, int> NewSmell )
	{
		if( ReceptorMoreIntense(ReceptorIndex, NewSmell.intensity) )
		{
			m_Receptors[ReceptorIndex] = NewSmell;
			Print("Smell "+NewSmell.name+" updated");
		}
		else 
		{
			Print("no need to update " +NewSmell.name);
		}
	}
	
	bool ReceptorMoreIntense( int ReceptorIndex, int NewIntensity)
	{
		if(m_Receptors[ReceptorIndex])
		{
			ref SmellProto<int, string, float, int, int, int, int, int> ExistingSmell = m_Receptors[ReceptorIndex];
			if(NewIntensity > ExistingSmell.intensity)
			{
				return true;
			}
		}
		return false;
	}
	
	bool SmellExists(int smellId, out int receptorIndex, out int freereceptorIndex)
	{
		
		ref SmellProto<int, string, float, int, int, int, int, int> Smell1 = m_Receptors[0];
		ref SmellProto<int, string, float, int, int, int, int, int> Smell2 = m_Receptors[1];
		ref SmellProto<int, string, float, int, int, int, int, int> Smell3 = m_Receptors[2];
		ref SmellProto<int, string, float, int, int, int, int, int> Smell4 = m_Receptors[3];
		
		if(Smell1) 
		{Print("SMELL1: " +Smell1.name+ ": "+Smell1.id + " ("+Smell1.intensity+") ") } 
		else 
		{Print("SMELL1: EMPTY");};
		if(Smell2)
		{Print("SMELL2: " +Smell2.name+ ": "+Smell2.id + " ("+Smell2.intensity+") ")  } 
		else 
		{Print("SMELL2: EMPTY");};
		if(Smell3) 
		{Print("SMELL3: " +Smell3.name+ ": "+Smell3.id + " ("+Smell3.intensity+") ")  } 
		else 
		{Print("SMELL3: EMPTY");};
		if(Smell4) 
		{Print("SMELL4: " +Smell4.name+ ": "+Smell4.id + " ("+Smell4.intensity+") ")  } 
		else 
		{Print("SMELL4: EMPTY");};
		
		for (int i = 0; i < m_Receptors.Count(); i++)
		{
			if(m_Receptors[i])
			{
				freereceptorIndex = -1;
			}
			else
			{
				freereceptorIndex = i;
			}
			
		}
		
		
		if ( (Smell1) && (Smell1.id == smellId) )
		{
			receptorIndex = 0;
			return true;
		}
		
		if ( (Smell2) && (Smell2.id == smellId) )
		{
			receptorIndex = 1;
			return true;
		}
		
		if ( (Smell3) && (Smell3.id == smellId) )
		{
			receptorIndex = 2;
			return true;
		}
		
		if ( (Smell4) && (Smell4.id == smellId) )
		{
			receptorIndex = 3;
			return true;
		}
		
		
		receptorIndex = -1;
		return false;
	}
	
	//void sendSmellsToReceptors(ref array<Object> nearest_objects)
	void sendSmellsToReceptors2()
	{
		int theTimestamp;
		DayZPlayer player = GetGame().GetPlayer();
		
		int hour;
		int minute;
		int second;
		int year;
		int month;
		int day;
		
		GetHourMinuteSecond(hour, minute, second);
		GetYearMonthDay(year, month, day);
		Print( (year + month + day).ToString() +  (hour/60 +  minute/60 +  second).ToString() );
		
		theTimestamp = ( (year + month + day).ToString() +  (day +  minute +  second).ToString() ).ToInt() ;
		// primary_receptor_array 4 slots
		// find smells in the nearest objects by type
		// found smell
		// smell exists on the receptors?
		/*
			
			int smellTimestamp;
			bool m_SmellExists = m_Receptors.Find(objectSmell, smellTimestamp);
			// new timestamp
			m_Receptors.Set(objectSmell, theTimestamp);
			// compare intensity
			
			// if bigger
			// new intensity 
			// old intensity
			// update with new intensity and timestamps
			// new smell
			// insert into receptors
			// refresh UI
			// if smell timestamp delta = 5
			// reduce intensity by 1
			// sort smells by intensity
			// update receptor1, receptor2, receptor3, receptor4
		*/
		
		m_Receptors.Insert(Smell.Animal);
		m_Receptors.Insert(Smell.AnimalDomestic);
		m_Receptors.Insert(Smell.AnimalDomestic);
		m_Receptors.Insert(Smell.AnimalWild);
		//Print(Smell.Animal.param1);
		//Print(m_Receptors[0].param1);
		// ref SmellProto<int, string, float, int, int, int, int, int> Smell1 = m_Receptors[0];
		// ref SmellProto<int, string, float, int, int, int, int, int> Smell2 = m_Receptors[1];
		// ref SmellProto<int, string, float, int, int, int, int, int> Smell3 = m_Receptors[2];
		// ref SmellProto<int, string, float, int, int, int, int, int> Smell4 = m_Receptors[3];
		//ReceptorsSendNewSmell
	}
}		

